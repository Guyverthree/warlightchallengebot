settings your_bot player1
settings opponent_bot player2
setup_map super_regions 1 5 2 2 3 5 4 3 5 7 6 2
setup_map regions 1 1 2 1 3 1 4 1 5 1 6 1 7 1 8 1 9 1 10 2 11 2 12 2 13 2 14 3 15 3 16 3 17 3 18 3 19 3 20 3 21 4 22 4 23 4 24 4 25 4 26 4 27 5 28 5 29 5 30 5 31 5 32 5 33 5 34 5 35 5 36 5 37 5 38 5 39 6 40 6 41 6 42 6
setup_map neighbors 1 2,4,30 2 4,3,5 3 5,6,14 4 5,7 5 6,7,8 6 8 7 8,9 8 9 9 10 10 11,12 11 12,13 12 13,21 14 15,16 15 16,18,19 16 17 17 19,20,27,32,36 18 19,20,21 19 20 20 21,22,36 21 22,23,24 22 23,36 23 24,25,26,36 24 25 25 26 27 28,32,33 28 29,31,33,34 29 30,31 30 31,34,35 31 34 32 33,36,37 33 34,37,38 34 35 36 37 37 38 38 39 39 40,41 40 41,42 41 42
pick_starting_regions 10000 3 8 10 12 20 18 22 23 29 35 41 42
settings starting_armies 5
update_map 6 player1 2 13 player1 2 26 player1 2 3 neutral 2 5 neutral 2 8 neutral 2 11 neutral 2 12 neutral 2 23 neutral 2 25 neutral 2
opponent_moves
go place_armies 2000
go attack/transfer 2000
update_map 1 player1 11 4 player1 1 7 player1 1 9 player1 10 10 player1 1 11 player1 1 12 player1 1 13 player1 1 15 player1 5 18 player1 1 21 player1 1 2 neutral 2 30 neutral 2 5 neutral 2 8 neutral 2 14 neutral 2 16 neutral 2 19 neutral 2 20 neutral 2 22 neutral 2 23 neutral 2 24 neutral 2
go place_armies 2000
go attack/transfer 2000
update_map 1 player1 3 2 player1 3 3 player1 3 4 player1 3 5 player1 5 7 player1 3 9 player1 3 10 player1 1 11 player1 1 12 player1 1 13 player1 1 18 player1 9 21 player1 1 22 player1 1 23 player1 1 24 player1 1 25 player1 1 26 player1 1 30 player1 9 36 player1 1 6 neutral 2 14 player2 13 8 neutral 2 15 player2 1 19 player2 1 20 player2 18 29 neutral 2 31 neutral 2 34 neutral 2 35 neutral 2 17 player2 1 32 player2 5 37 neutral 2
go place_armies 2000
go attack/transfer 2000