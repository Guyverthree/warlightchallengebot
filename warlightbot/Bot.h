
#ifndef BOT_H
#define BOT_H
#include <iostream>
#include <stdio.h>

#include <fstream>
#include <iostream>
#include <vector>
#include "Parser.h"
#include "MapData.h"

using namespace std;

/*
enum conquestRegionNames{
	1: 'Alaska',
	2: 'Northwest Territory',
	3: 'Greenland',
	4: 'Alberta',
	5: 'Ontario',
	6: 'Quebec',
	7: 'Western United States',
	8: 'Eastern United States',
	9: 'Central America',
	10: 'Venezuela',
	11: 'Peru',
	12: 'Brazil',
	13: 'Argentina',
	14: 'Iceland',
	15: 'Great Britain',
	16: 'Scandinavia',
	17: 'Ukraine',
	18: 'Western Europe',
	19: 'Northern Europe',
	20: 'Southern Europe',
	21: 'North Africa',
	22: 'Egypt',
	23: 'East Africa',
	24: 'Congo',
	25: 'South Africa',
	26: 'Madagascar',
	27: 'Ural',
	28: 'Siberia',
	29: 'Yakutsk',
	30: 'Kamchatka',
	31: 'Irkutsk',
	32: 'Kazakhstan',
	33: 'China',
	34: 'Mongolia',
	35: 'Japan',
	36: 'Middle East',
	37: 'India',
	38: 'Siam',
	39: 'Indonesia',
	40: 'New Guinea',
	41: 'Western Australia',
	42: 'Eastern Australia'
};*/

typedef struct Move
{
	int armies;
	int goingfrom;
	int goingto;
	bool attack;
	int value; // non 0 if in attack mode to rate attacks
} MOVE, LPMOVE;

typedef struct Placement
{
	int region;
	int armies;
	int value; // value of attacks
} PLACEMENT, LPPLACEMENT;

class Bot
{
	friend class Parser;
	Parser parser;

	MapData& theMap; // refrence to the map cannot be NULL

	ifstream in;
	string botName;
	string opponentBotName;
	vector<int> startingRegionsreceived;
	vector<MOVE> moveslist;
	vector<PLACEMENT> placementlist;
	vector<int> startingpick;
	int armiesLeft;
	string phase;
	int turnNo;

	void addPlacement(int region,int armies);
	void addMovement(int origin, int destination,int armies,bool attack);

	void placeArmies();
	vector<int>& pickStart(vector<int>& picklist,vector<int>& startingpicks);

	public:
		Bot(MapData& map);
		virtual ~Bot();

	void playGame();    //plays a single game of Warlight
	void makeMoves();   //makes moves for a single turn
	void endTurn();     //indicates to the engine that it has made its moves

	void setBotName(string name);
	void setOpponentBotName(string name);
	void setArmiesLeft(int nbArmies);
	void addStartingRegion(unsigned noRegion);
	void startDelay(int delay);
	void setPhase(string pPhase);
	void executeAction();

	//game state functions
	int incTurn(){return ++turnNo;}

	private:
};

#endif // BOT_H
