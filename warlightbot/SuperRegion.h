#ifndef SUPERREGION_H_INCLUDED
#define SUPERREGION_H_INCLUDED
#include <vector>
#include "Region.h"
using namespace std;
class SuperRegion
{

    std::vector<int> regions;
    int nbRegions;
    int reward;
	int ownedregions; // regions currently owned in super region
    public:
        SuperRegion();
        SuperRegion(int pReward);
        virtual ~SuperRegion();
        void addRegion(int noRegion);
        int getNbRegions();
		int GetReward(){return reward;};
		int GetOwnedRegions(){return ownedregions;};
		void IncOwnedRegions(){ownedregions++;};
		void ClearOwnedRegions(){ownedregions=0;};
		std::vector<int>& GetRegions(){ return regions;};
    protected:
    private:
};



#endif // SUPERREGION_H_INCLUDED
