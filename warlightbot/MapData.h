#ifndef MAPDATA_H
#define MAPDATA

#include "Region.h"
#include "SuperRegion.h"

class MapData
{
private:	
	void isfullyconnected(Region& region); // maybe change to a vector of superregions to track more than one
	void valueof(Region& region); // computes the raw value of the region in terms of armies per turn

	// current turns map
	vector<int> boarders;				//list of boarders (regions with attack options)
	vector<int> coreregions;			//list of core regions (non boarders)
	vector<int> ownedregions;			//list of owned regions
	vector<int>	attackoptions;			//list of attack options
	vector<int> neutralregions;			// list of neutral regions
	vector<int> enemyregions;			// list of enemy regions

	//next turns map after my moves
	vector<int> nextboarders;			//list of boarders (regions with attack options)
	vector<int> nextcoreregions;		//list of core regions (non boarders)
	vector<int> nextownedregions;		//list of owned regions
	vector<int>	nextattackoptions;		//list of attack options
	vector<int> nextneutralregions;		// list of neutral regions
	vector<int> nextenemyregions;		// list of enemy regions
	
	
public:
	MapData();
	virtual ~MapData();

	//TODO make private and make zero indexed vectors.
	vector<Region> regions;
	vector<SuperRegion> superRegions;

	vector<int>& getBoarders()		{return boarders;};
	bool isBoarder(int region);// return true if it boarders somewhere i can attack
	vector<int>& getOwnedRegions()	{return ownedregions;};
	vector<int>& getAttackOptions()	{return attackoptions;};
	vector<int>& getCoreRegions()	{return coreregions;};
	bool sameSuper(int region,int region2);// are they in the same super region

	vector<int> StartRegions(vector<int> &startregions);

	bool isfullyconnected(int region){return regions[region].getConnected();}; // access for isfullyconnected
	int requiredarmiestoattack(int region);
	int requiredarmiestodefend(int region);
	int findBoarder(int origin); // return the region to move to for nearest boarder
	void sortByValue(vector<int>& regionlist);
	void sortByArmies(vector<int>& regionlist);
	
	
	// process for update map
	void UpdateBorders(string botname);
	void updateRegionNeutral(unsigned noRegion, string playerName, int nbArmies,int turn);
	void updateRegionEnemy(unsigned noRegion, string playerName, int nbArmies,int turn);
	void updateRegionOwned(unsigned noRegion, string playerName, int nbArmies,int turn);
	void addArmies(unsigned noRegion,int nbArmies);
	void moveArmies(unsigned noRegion,unsigned toRegion,int nbArmies);
	void ProcessMapUpdate(); // called each turn to re-evaluate map.

	//setup the map data
	void addRegion(unsigned noRegion, unsigned noSuperRegion);
	void addSuperRegion(unsigned noSuperRegion, int reward);
	void addNeighbors(unsigned noRegion, unsigned Neighbors);
	void ProcessMapRaw(); // called at setup

	

};

#endif // MAPDATA_H