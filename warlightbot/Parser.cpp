#include "Parser.h"
#include <iostream>
#include <stdlib.h>
#include "Bot.h"

using namespace std;

Parser::Parser()
{
	turn = 0;
	//ctor
}

Parser::~Parser()
{
	//dtor
}

void Parser::initParser(Bot* bot,MapData* map)
{
	theBot = bot;
	theMap = map;
#ifdef TESTMODE
	// opens test file
	testinput.open("..//testfiles//test2.txt");
	if (testinput.is_open())
	{
		cout << "in test mode";
	}
#endif
}

string Parser::getInput()
{
	string output;
#ifndef TESTMODE
	cin >> output;
#else
	testinput >> output;
#endif
	return output;
}

int Parser::getInputint()
{
	int output;
#ifndef TESTMODE
	cin >> output;
#else
	testinput >> output;
#endif
	return output;
}

int Parser::getInputpeek()
{
#ifndef TESTMODE
	return cin.peek();
#else
	return testinput.peek();
#endif
}



void Parser::parseInput()
{
	string inputType;
	while (true)
	{
		inputType = getInput();
		if (inputType == "setup_map")
			parseSetup_Map();
		else if (inputType == "pick_starting_regions")
			parseStarting_Regions();
		else if (inputType == "pick_starting_region")
			parseStarting_RegionsWarlight2();
		else if (inputType == "settings")
			parseSettings();
		else if (inputType == "update_map")
			parseUpdate_Map();
		else if (inputType == "opponent_moves")
			parseOpponent_Moves();
		else if (inputType == "go")
			parseGo();
		theBot->executeAction();
	}
}

void Parser::parseSetup_Map()
{
#ifdef DEBUG_PRINT
	 cout <<"parseSetupMap\n";
#endif // DEBUG_PRINT
	string setupType;
	setupType = getInput();
	if (setupType == "super_regions")
		parseSuper_Regions();
	if (setupType == "regions")
		parseRegions();
	if (setupType == "neighbors")
		parseNeighbors();
}

void Parser::parseStarting_RegionsWarlight2()
{
#ifdef DEBUG_PRINT
	cout << "parseStarting_RegionsWarlight2\n";
#endif // DEBUG_PRINT
	int region;
	int delay;
	delay = getInputint();
	theBot->startDelay(delay);
	while(true)
	{
		region = getInputint();
		theBot->addStartingRegion(region);
		if (getInputpeek() == '\n')
			break;
	}
	theBot->setPhase("pickPreferredRegion");
}

void Parser::parseStarting_Regions()
{
#ifdef DEBUG_PRINT
	cout << "parseStartingRegions\n";
#endif // DEBUG_PRINT
	int region;
	int delay;
	delay = getInputint();
	theBot->startDelay(delay);
	while(true)
	{
		region = getInputint();
		theBot->addStartingRegion(region);
		if (getInputpeek() == '\n')
			break;
	}
	theBot->setPhase("pickPreferredRegion");
}

void Parser::parseSettings()
{
#ifdef DEBUG_PRINT
	cout << "parseSettings\n";
#endif // DEBUG_PRINT
	string settingType = getInput();
	if (settingType == "your_bot")
	{
		theBot->setBotName(getInput());
	}
	if (settingType == "opponent_bot")
	{
		theBot->setOpponentBotName(getInput());
	}
	if (settingType == "starting_armies")
	{
		theBot->setArmiesLeft(getInputint());
#ifdef DEBUG_PRINT
		cout << "settings starting_armies " << nbArmies << "\n";
#endif // DEBUG_PRINT
	}
}

void Parser::parseUpdate_Map()
{
#ifdef DEBUG_PRINT
	cout <<"parseUpdate_Map\n";
#endif // DEBUG_PRINT
	string playerName;
	int noRegion, nbArmies;
	theMap->getOwnedRegions().clear();
	theMap->getAttackOptions().clear();
	turn = theBot->incTurn();

	while (true)
	{
		noRegion = getInputint();
		playerName = getInput();
		nbArmies = getInputint();
		if (theBot->botName == playerName)
		{
			theMap->updateRegionOwned(noRegion,playerName,nbArmies,turn);
		}
		else if (theBot->opponentBotName == playerName)
		{
			theMap->updateRegionEnemy(noRegion,playerName,nbArmies,turn);
		}
		else
		{
			theMap->updateRegionNeutral(noRegion,playerName,nbArmies,turn);
		}
		if (getInputpeek() == '\n')
			break;
	}

	theMap->UpdateBorders(theBot->botName);
	theMap->ProcessMapUpdate();
}

void Parser::parseOpponent_Moves()
{

#ifdef DEBUG_PRINT
	cout << "parseOpponent_Moves\n";
#endif // DEBUG_PRINT
	string playerName, action;
	int noRegion, nbArmies, toRegion;
	while (true)
	{
		if (getInputpeek() == '\n')
			break;
		playerName = getInput();
		action = getInput();
		if (action == "place_armies")
		{
			noRegion = getInputint();
			nbArmies = getInputint();
			theMap->addArmies(noRegion,nbArmies);
		}
		if (action == "attack/transfer")
		{
			noRegion = getInputint();
			toRegion = getInputint();
			nbArmies = getInputint();
			theMap->moveArmies(noRegion,toRegion,nbArmies);
	   }
		
	}
}

void Parser::parseGo()
{
	string phase;
	int delay;
	phase = getInput();
	delay = getInputint();
	theBot->startDelay(delay);
	theBot->setPhase(phase);
}

void Parser::parseSuper_Regions()
{
	int super,reward;
#ifdef DEBUG_PRINT
	cout << "parseSuperRegions\n";
#endif // DEBUG_PRINT

	while(true)
	{
		super = getInputint();
		reward = getInputint();
		 theMap->addSuperRegion(super,reward);
		 if (getInputpeek() == '\n')
			break;
	}
}

void Parser::parseRegions()
{
	int super,region;
#ifdef DEBUG_PRINT
	cout << "parseRegions\n";
#endif // DEBUG_PRINT

	while(true)
	{
		region = getInputint();
		super = getInputint();
		theMap->addRegion(region,super);
		 if (getInputpeek() == '\n')
			break;
	}
}

void Parser::parseNeighbors()
{
#ifdef DEBUG_PRINT
	cout <<  "parseNeighbors\n";
#endif // DEBUG_PRINT
	int region;
	string neighbors;
	vector<string> neighbors_flds;
	while(true)
	{
		region = getInputint();
		neighbors = getInput();
		neighbors_flds = splitString(neighbors, neighbors_flds, ',');
		for (unsigned i = 0; i < neighbors_flds.size(); i++)
			theMap->addNeighbors(region, atoi(neighbors_flds[i].c_str()));
		 if (getInputpeek() == '\n')
			break;
	}
	neighbors_flds.clear();
}

vector<string>& Parser::splitString(string String, vector<string>& flds, char delim)
{
	if (!flds.empty())
		flds.clear();
	string buf = "";
	unsigned i = 0;
	while (i < String.length())
	{
		if (String[i] != delim)
			buf += String[i];
		else
		{
			flds.push_back(buf);
			buf = "";
		}
		i++;
	}
	if (!buf.empty())
		flds.push_back(buf);
	return flds;
}


