#include <iostream>
#include <sstream>
#include <string>
#include "Bot.h"


Bot::Bot(MapData& map):theMap(map)
{
   // printf("ctor\n");
	armiesLeft = 0;
}

Bot::~Bot()
{
	 //printf("dtor\n");
	//dtor
}

void Bot::playGame()
{
	
	parser.initParser(this,&theMap);
	parser.parseInput();
}    //plays a single game of Warlight

vector<int>& Bot::pickStart(vector<int>& picklist,vector<int>& startingpicks)
{
	theMap.ProcessMapRaw();

	if (!startingpicks.empty())
	{
		startingpicks.clear();
	}

	for(std::vector<int>::iterator itr = picklist.begin() , itrend = picklist.end(); itr!=itrend; ++itr)
	{
		// stuff for bot 2.
		// lower- ignore a pick in a super region that has no bonus.
		// lower- try to ignore regions in superregions with wasteland in
		// lower- if it's a region with only 1 neighbour

		// can i take the whole super region from it.
		if (theMap.isfullyconnected(*itr))
		{
			startingpicks.push_back(*itr);
			// can i pick another of that superregion.
			/*for(std::vector<int>::iterator itr2 = picklist.begin() , itrend2 = picklist.end(); itr2!=itrend; ++itr2)
			{
				if ((*itr != *itr2) && (theMap.sameSuper(*itr,*itr2)))
				{
					startingpicks.push_back(*itr2);
				}
			}*/
			continue;
		}
		
		// can i pick a region that boarders that superregion
		// repeat
	}




	for(std::vector<int>::iterator itr = picklist.begin() , itrend = picklist.end(); itr!=itrend; ++itr) 
	{
		bool bpicked = false;
		for(std::vector<int>::iterator itrneigh = startingpicks.begin() , itrneightend = startingpicks.end(); itrneigh!=itrneightend; ++itrneigh) 
		{
			if ((*itr) == (*itrneigh))
			{
				bpicked = true;
			}
		}
		if (bpicked == false)
		{
			// region is not picked
			startingpicks.push_back((*itr));
		}
	}

	return startingpicks;
}

void Bot::addPlacement(int region,int armies)
{
	PLACEMENT reinforce = {0};
	reinforce.armies = armies;
	reinforce.region = region;
	placementlist.push_back(reinforce);
	theMap.addArmies(region,armies);
}

void Bot::addMovement(int origin, int destination,int armies,bool attack)
{
	// can move to a boarder let's go
	MOVE move = {0};
	move.armies = armies;
	move.goingfrom = origin;
	move.goingto = destination;
	move.attack = attack;
	moveslist.push_back(move);

	theMap.addArmies(origin,-armies);
}

void Bot::placeArmies()
{
	placementlist.clear(); //TODO make this list a list of options rathen than a single list

	/*vector<int>& boarders = theMap.getBoarders();
	for(int i = 0; i < boarders.size(); i++)
	{
		int origin = boarders[i];
		std::vector<int> neighbors = theMap.regions[origin].getNeighbors();
		for(std::vector<int>::iterator itrniegh = neighbors.begin() , itrneighend = neighbors.end(); itrniegh != itrneighend; ++itrniegh) 
		{
			if (theMap.regions[*itrniegh].getOwner() == opponentBotName)
			{
				addPlacement(origin,1);
				armiesLeft--;
				break;
			}
		}
		if (armiesLeft == 0)
		{
			break;
		}
	}*/
	
	vector<int>& attackoptions = theMap.getAttackOptions();
	// add the right amount of armies or at least try
	for(int i = 0; i < attackoptions.size(); i++)
	{
		int target = attackoptions[i];
		int armiesneeded = theMap.requiredarmiestoattack(target);
		std::vector<int> neighbors = theMap.regions[target].getNeighbors();
		theMap.sortByArmies(neighbors);
		for(std::vector<int>::iterator itrniegh = neighbors.begin() , itrneighend = neighbors.end(); itrniegh != itrneighend; ++itrniegh) 
		{
			if (theMap.regions[*itrniegh].getOwner() == botName)
			{
				if ((theMap.regions[*itrniegh].getArmies() - 1 < armiesneeded))
				{
					int reqArmies = armiesneeded - (theMap.regions[*itrniegh].getArmies() - 1);
					if (armiesLeft >= reqArmies)
					{
						addPlacement(*itrniegh,reqArmies);
						armiesLeft = armiesLeft - reqArmies;
					}
					// if the place with the most armies does not have enough don't bother trying the rest
					break;
				}

			}
		}
		if (armiesLeft == 0)
		{
			break;
		}
	}

	//find places to add armies
	for(int i = 0; i < attackoptions.size(); i++)
	{
		int target = attackoptions[i];
		std::vector<int> neighbors = theMap.regions[target].getNeighbors();
		for(std::vector<int>::iterator itrniegh = neighbors.begin() , itrneighend = neighbors.end(); itrniegh != itrneighend; ++itrniegh) 
		{
			if (theMap.regions[*itrniegh].getOwner() == botName)
			{
				addPlacement(*itrniegh,armiesLeft);
				armiesLeft = 0;
				break;
			}
		}
		if (armiesLeft == 0)
		{
			break;
		}
	}

}


void Bot::makeMoves()
{
	moveslist.clear(); //remove last moves
	//process map

	/// now move from core systems to boarders
	for(std::vector<int>::iterator itr = theMap.getCoreRegions().begin() , itrend = theMap.getCoreRegions().end(); itr!=itrend; ++itr) 
	{
		if (theMap.regions[*itr].getArmies() > 1)
		{
			int destination = theMap.findBoarder(*itr);
			addMovement(*itr,destination,theMap.regions[*itr].getArmies() - 1,false);
		}
	}

	vector<int>& attackoptions = theMap.getAttackOptions();
	//figure out attacks
	for(int i = 0; i < attackoptions.size(); i++)
	{
		int target = attackoptions[i];
		std::vector<int> neighbors = theMap.regions[target].getNeighbors();
		for(std::vector<int>::iterator itrniegh = neighbors.begin() , itrneighend = neighbors.end(); itrniegh != itrneighend; ++itrniegh) 
		{
			if (theMap.regions[*itrniegh].getOwner() == botName)
			{
				int armies =  theMap.requiredarmiestoattack(target);
				if (theMap.regions[*itrniegh].getArmies() > armies)
				{
					// this should result in a positive attack
					addMovement(*itrniegh,target,armies,true);
					break;
				}
			}
		}
	}

}   //makes moves for a single turn
void Bot::endTurn()
{

}     //indicates to the engine that it has made its move

void Bot::setBotName(string name)
{
	botName = name;
}
void Bot::setOpponentBotName(string name)
{
	opponentBotName = name;
}

void Bot::setArmiesLeft(int nbArmies)
{
	armiesLeft = nbArmies;
}

void Bot::addStartingRegion(unsigned noRegion)
{
	 startingRegionsreceived.push_back(noRegion);
}

void Bot::startDelay(int delay)
{

}

void Bot::setPhase(string pPhase)
{
	phase=pPhase;
}
void Bot::executeAction()
{
	if (phase=="")
		return;
	if (phase == "pickPreferredRegion")
	{
		vector<int> bestpicks;
		pickStart(startingRegionsreceived,bestpicks);
		unsigned i,nbAns=0;
		for (i = 0; i< bestpicks.size() && nbAns<6; i++)
		{
			cout << bestpicks[i];
			nbAns++;
			if (nbAns < 6)
				cout << " ";
			else
			{
				cout << "\n";
				break;
			}
		}
	}
	if (phase == "place_armies")
	{
		placeArmies();
		if (placementlist.empty())
		{//default
			cout << botName << " place_armies " << theMap.getOwnedRegions()[1] << " " << armiesLeft << endl;
		}
		else
		{
			ostringstream placement;
			for(std::vector<PLACEMENT>::iterator itr = placementlist.begin() , itrend = placementlist.end(); itr!=itrend; ++itr)
			{
				placement << botName << " place_armies " << (*itr).region << " " << (*itr).armies;
				// add seperator
				if( (itr + 1) != itrend)
				{
					placement << ", ";
				}
			}
			cout << placement.str() << endl;
		}
	}
	if (phase == "attack/transfer")
	{
		makeMoves();
		if (!moveslist.empty())
		{
			ostringstream moves;
			for(std::vector<MOVE>::iterator itr = moveslist.begin() , itrend = moveslist.end(); itr!=itrend; ++itr)
			{
				// command
				moves << botName << " attack/transfer " << (*itr).goingfrom << " " <<  (*itr).goingto << " " <<  (*itr).armies;
				// add seperator
				if( (itr + 1) != itrend)
				{
					moves << ", ";
				}
			}
			cout << moves.str() << endl;
		}
		else
		{
			cout << "No moves\n" ;
		}
	}
	phase.clear();
}
