#include "SuperRegion.h"

//TODO:JAMES remove this and rewrite the parser to allow 0 index.
SuperRegion::SuperRegion()
{
	nbRegions = 0;
	reward = 0;
	ownedregions = 0;
    //ctor
}

SuperRegion::~SuperRegion()
{
    //dtor
}

SuperRegion::SuperRegion(int pReward)
{
	nbRegions = 0;
	ownedregions = 0;
    reward=pReward;
}
void SuperRegion::addRegion(int noRegion)
{
    regions.push_back(noRegion);
    nbRegions++;
}
int SuperRegion::getNbRegions()
{
    return regions.size();
}
