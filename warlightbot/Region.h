#ifndef REGION_H
#define REGION_H
#include <vector>
#include <string>
using namespace std;
class Region
{

	std::vector<int> neighbors;
	int id;
	int nbNeighbors;// could be redundant
	int superRegion;
	string owner; // change to an int.
	int armies;
	int visible; // is the map visible
	double value; // value of the region in raw terms of armies
	bool isfullyconnected;
	public:
		Region();
		Region(int pId,int superRegion);
		virtual ~Region();
		void addNeighbors(int Neighbors);
		void setArmies(int nbArmies);
		void setOwner(string owner);
		int getArmies();
		string getOwner();
		int getSuperRegion();
		vector<int>& getNeighbors();
		int getId(){return id;};

		void setConnected(bool connected){isfullyconnected = connected;};
		bool getConnected(){return isfullyconnected;};

		void setVisible(int ivisible){visible = ivisible;};
		int getVisible(){return visible;};

		void setValue(double dvalue){value = dvalue;};
		double getValve(){return value;};
	protected:
	private:
};

#endif // REGION_H
