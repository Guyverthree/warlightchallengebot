#include "Region.h"
#include <stdio.h>
#include <iostream>
#include "SuperRegion.h"
using namespace std;
Region::Region()
{
	//ctor
	id = 0;
	superRegion = 0;
	armies = 0;
	owner = "\0";
	nbNeighbors = 0;
}

Region::Region(int pId, int pSuperRegion)
{
	superRegion = pSuperRegion;
	id = pId;
	armies = 0;
	owner = "\0";
	nbNeighbors = 0;
}

Region::~Region()
{
	//dtor
}
void Region::addNeighbors(int Neighbors)
{
	neighbors.push_back(Neighbors);
	nbNeighbors++;
}

void Region::setArmies(int nbArmies)
{
	armies =nbArmies;
}
void Region::setOwner(string pOwner)
{
	owner = pOwner;
}
int Region::getArmies()
{
	return armies;
}
string Region::getOwner()
{
	return owner;
}

int Region::getSuperRegion()
{
	return superRegion;
}

vector<int>& Region::getNeighbors()
{
	return neighbors;
}

