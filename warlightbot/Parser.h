#ifndef PARSER_H_INCLUDED
#define PARSER_H_INCLUDED
#include <stdio.h>
#include <fstream>

#include <string>
#include <vector>
using namespace std;
class Bot;
class MapData;
class Parser
{
	Bot* theBot;
	MapData* theMap;
	ifstream testinput;
	int turn; // turn counter

	public:
		Parser();
		virtual ~Parser();
		void initParser(Bot* bot,MapData* map);
		void parseInput();
		void parseSetup_Map();
		void parseStarting_Regions();
		void parseStarting_RegionsWarlight2();
		void parseSettings();
		void parseUpdate_Map();
		void parseOpponent_Moves();
		void parseGo();
		void parseSuper_Regions();
		void parseRegions();
		void parseNeighbors();

		string getInput();
		int getInputint();
		int getInputpeek();
	protected:
	private:
		vector<string>& splitString(string String, vector<string>& flds, char delim);
};

#endif // PARSER_H_INCLUDED
