#include "MapData.h"
#include <list>

#define OPPONENTARMIES 5 // replace this with a proper guess for now it's 5.

#define CEILING(X) (X-(int)(X) > 0 ? (int)(X+1) : (int)(X))

MapData::MapData()
{
   // printf("ctor\n");
}

MapData::~MapData()
{
	 //printf("dtor\n");
	//dtor
}

void MapData::valueof(Region& region)
{
	double value = 0;
	int reward = superRegions[region.getSuperRegion()].GetReward();
	int size = (superRegions[region.getSuperRegion()].GetRegions().size() - superRegions[region.getSuperRegion()].GetOwnedRegions());
	if (size > 0)
	{
		value = ((double)reward / (double)size);
	}
	else
	{
		value = 0.3;
	}
	if (region.getConnected())
	{
		value = value + 0.3; //TODO figure out a better way of storing this
	}
	region.setValue(value);
}

void MapData::isfullyconnected(Region& region) //does region border entire super regions
{	
	int id = region.getId();
	std::vector<int> neighbors = regions[id].getNeighbors(); 
	for(std::vector<SuperRegion>::iterator itrsuper = superRegions.begin() , itssuperrend = superRegions.end(); itrsuper!=itssuperrend; ++itrsuper)
	{
		// only compare super regions with less size than neightbours +1
		if (((*itrsuper).getNbRegions() != 0 ) && ( (*itrsuper).getNbRegions() <= (neighbors.size() + 1)))
		{
			//assume connected prove it's not
			bool bisconnected = true;
			std::vector<int> regions = (*itrsuper).GetRegions();
			// are all regions neighbours
			for(std::vector<int>::iterator itr = regions.begin() , itrend = regions.end(); itr!=itrend; ++itr) 
			{
				bool bisneighbour = false;
				if ((*itr) != id)
				{
					for(std::vector<int>::iterator itrneigh = neighbors.begin() , itrneightend = neighbors.end(); itrneigh!=itrneightend; ++itrneigh) 
					{
						if ((*itr) == (*itrneigh))
						{
							bisneighbour = true;
							break;
						}
					}
				}
				else
				{
					bisneighbour = true;
				}
				if (bisneighbour == false)
				{
					// if one region not a neightbour super region is not fully connected to this region.
					bisconnected = false;
					break;
				}
			}
			if (bisconnected)
			{
				region.setConnected(true);
				return;
			}
		}
	}
	region.setConnected(false);
	return;
}

int MapData::requiredarmiestoattack(int region)
{
	/*bool enemyregion = false;
	for(std::vector<int>::iterator itr = enemyregions.begin() , itrend = enemyregions.end(); itr!=itrend; ++itr)
	{
		if (region == (*itr))
		{
			enemyregion = true;
			break;
		}
	}*/
	//if (!enemyregion)
	//{
	// only neturals right now
	return CEILING(regions[region].getArmies() * 1.67);
	//return CEILING((regions[region].getArmies() + OPPONENTARMIES) * 1.67); // TODO:find out the proper equation for this and add bots armies
}

int MapData::requiredarmiestodefend(int region)
{
	int biggeststack = 0;
	std::vector<int> neighbours = regions[region].getNeighbors();
	for(std::vector<int>::iterator itrneigh = neighbours.begin() , itrneighend = neighbours.end(); itrneigh!=itrneighend; ++itrneigh)
	{
		for(std::vector<int>::iterator itr = enemyregions.begin() , itrend = enemyregions.end(); itr!=itrend; ++itr)
		{
			if ((*itrneigh) == (*itr))
			{
				if ((regions[*itr].getArmies()) > biggeststack)
				{
					// store the largest enemy force that can attack this region.
					biggeststack = regions[*itr].getArmies();
				}
			}
		}
	}
	if (biggeststack == 0)
	{
		// only neturals right now
		return 1;
	}
	return biggeststack;// + OPPONENTARMIES; //TODO add the estimated enemy bots armies here.
}

// process the Map to start with.
void MapData::ProcessMapRaw()
{
	//process map and find the important network regions
	
	// important
	// if region blocks another region
	// region allows a whole super region to be taken from i
	for(std::vector<Region>::iterator itr = regions.begin() , itrend = regions.end(); itr!=itrend; ++itr)
	{
		if ((*itr).getId() != 0)
		{
			this->isfullyconnected(*itr);
			this->valueof(*itr);
		}
	}
	// if region joins super regions

	// calulate super regions importance
	// value - total
	// territories - total - more generally harder to hold
	// boarders - more boaders bad

	// unimportant
	// if the super region has no bonus ignore regions until the end
}

void MapData::ProcessMapUpdate()
{
	for(std::vector<SuperRegion>::iterator itr = superRegions.begin() , itrend = superRegions.end(); itr!=itrend; ++itr)
	{
		(*itr).ClearOwnedRegions();
	}
	// for each of my owned regions
	for(std::vector<int>::iterator itr = ownedregions.begin() , itrend = ownedregions.end(); itr!=itrend; ++itr)
	{
		int super = regions[*itr].getSuperRegion();
		superRegions[super].IncOwnedRegions();
	}

	// for each attackoption re-compute the value
	for(std::vector<int>::iterator itr = attackoptions.begin() , itrend = attackoptions.end(); itr!=itrend; ++itr) 
	{
		this->valueof(regions[*itr]);
	}
	this->sortByValue(attackoptions);
}


void MapData::sortByValue(vector<int>& regionlist)
{
	for(std::vector<int>::iterator itr = regionlist.begin() , itrend = regionlist.end(); itr!=itrend; ++itr) 
	{
		for(std::vector<int>::iterator itrlittle = regionlist.begin() , itrlitteend = regionlist.end(); itrlittle!=itrlitteend; ++itrlittle) 
		{
			if (regions[*itrlittle].getValve() < regions[*itr].getValve())
			{
				swap(*itr,*itrlittle);
			}
		}
	}
}

void MapData::sortByArmies(vector<int>& regionlist)
{
	for(std::vector<int>::iterator itr = regionlist.begin() , itrend = regionlist.end(); itr!=itrend; ++itr) 
	{
		for(std::vector<int>::iterator itrlittle = regionlist.begin() , itrlitteend = regionlist.end(); itrlittle!=itrlitteend; ++itrlittle) 
		{
			if (regions[*itrlittle].getArmies() < regions[*itr].getArmies())
			{
				swap(*itr,*itrlittle);
			}
		}
	}
}

bool MapData::isBoarder(int region)
{
	for(std::vector<int>::iterator itr = boarders.begin() , itrend = boarders.end(); itr!=itrend; ++itr) 
	{
		// it is a boarder region
		if (region == (*itr))
		{
			return true;
		}
	}
	return false;
}

typedef struct vertex
{
	int region;
	int moves;
	int previousnode;
} VERTEX, LPVERTEX;

int MapData::findBoarder(int origin)
{
	//TODO breath first search of the map
	std::list<VERTEX> search;
	std::vector<VERTEX> searched;
	bool bfound = false;
	VERTEX start;
	start.region = origin;
	start.moves = 0;
	start.previousnode = 0;
	search.push_back(start);

	while((search.size() != 0) && (!bfound))
	{
		vertex current = search.front();
		searched.push_back(current);
		search.pop_front();
		std::vector<int> neighbors = regions[current.region].getNeighbors();
		for(std::vector<int>::iterator itrniegh = neighbors.begin() , itrneighend = neighbors.end(); itrniegh!=itrneighend; ++itrniegh)
		{
			if (isBoarder(*itrniegh))
			{
				vertex v;
				v.region = *itrniegh;
				v.moves = current.moves + 1;
				v.previousnode = current.region;
				searched.push_back(v);
				bfound = true;
				break;
			}
			bool bsearched = false;
			for(std::vector<VERTEX>::iterator itr = searched.begin() , itrend = searched.end(); itr!=itrend; ++itr)
			{
				if (*itrniegh == (*itr).region)
				{
					bsearched = true;
					break;
				}
			}
			if (!bsearched)
			{
				bool binlist = false;
				for(std::list<VERTEX>::iterator itr = search.begin() , itrend = search.end(); itr!=itrend; ++itr)
				{
					if (*itrniegh == (*itr).region)
					{
						binlist = true;
						break;
					}
				}
				if (!binlist)
				{
					vertex v;
					v.region = *itrniegh;
					v.moves = current.moves + 1;
					v.previousnode = current.region;
					search.push_back(v);
				}
			}
		}
	}

	//TODO search list contains best route do a reverse search to get best route
	vertex nextmove = searched.back();
	searched.pop_back();
	while(nextmove.moves != 1)
	{
		vertex previous = searched.back();
		searched.pop_back();

		std::vector<int> neighbors = regions[nextmove.region].getNeighbors();
		for(std::vector<int>::iterator itrniegh = neighbors.begin() , itrneighend = neighbors.end(); itrniegh!=itrneighend; ++itrniegh)
		{
			if ((*itrniegh) == (previous.region))
			{
				nextmove =previous;
				break;
			}
		}
	}

	
	return nextmove.region;
}

bool MapData::sameSuper(int region,int region2)
{
	if (regions[region].getSuperRegion() == regions[region2].getSuperRegion())
	{
		return true;
	}
	return false;
}

void MapData::UpdateBorders(string botName)
{
	boarders.clear();
	coreregions.clear();
	for(std::vector<int>::iterator itrowned = ownedregions.begin() , itrownedend = ownedregions.end(); itrowned!=itrownedend; ++itrowned) 
	{
		bool bBoarder = false;
		std::vector<int> neighbors = regions[*itrowned].getNeighbors();
		for(std::vector<int>::iterator itr = neighbors.begin() , itrend = neighbors.end(); itr!=itrend; ++itr) 
		{
			// it is a boarder region
			if (regions[*itr].getOwner() != botName)
			{
				boarders.push_back(*itrowned);
				bBoarder = true;
				break;
			}
		}
		if (!bBoarder)
		{
			coreregions.push_back(*itrowned);
		}
	}
}

void MapData::updateRegionNeutral(unsigned noRegion, string playerName, int nbArmies,int turn)
{
	regions[noRegion].setArmies(nbArmies);
	regions[noRegion].setOwner(playerName);
	regions[noRegion].setVisible(turn);
	attackoptions.push_back(noRegion);
	neutralregions.push_back(noRegion);
}
void MapData::updateRegionEnemy(unsigned noRegion, string playerName, int nbArmies,int turn)
{
	regions[noRegion].setArmies(nbArmies);
	regions[noRegion].setOwner(playerName);
	regions[noRegion].setVisible(turn);
	attackoptions.push_back(noRegion);
	enemyregions.push_back(noRegion);
}
void MapData::updateRegionOwned(unsigned noRegion, string playerName, int nbArmies,int turn)
{
	regions[noRegion].setArmies(nbArmies);
	regions[noRegion].setOwner(playerName);
	regions[noRegion].setVisible(turn);
	ownedregions.push_back(noRegion);
}

void MapData::addRegion(unsigned noRegion, unsigned noSuperRegion)
{
	while (  regions.size() <= noRegion)
		{
			regions.push_back(Region());
		}
	regions[noRegion]  = Region(noRegion, noSuperRegion);
	superRegions[noSuperRegion].addRegion(noRegion);
}

void MapData::addNeighbors(unsigned noRegion, unsigned neighbors)
{
	regions[noRegion].addNeighbors(neighbors) ;
	regions[neighbors].addNeighbors(noRegion) ;
}

void MapData::addSuperRegion(unsigned noSuperRegion, int reward)
{
	while (  superRegions.size() <=  noSuperRegion)
		{
			superRegions.push_back(SuperRegion());
		}
	superRegions[noSuperRegion]  = SuperRegion(reward);
}

void MapData::addArmies(unsigned noRegion,int nbArmies)
{
	regions[noRegion].setArmies(regions[noRegion].getArmies() + nbArmies);
}

void MapData::moveArmies(unsigned noRegion,unsigned toRegion,int nbArmies)
{
	if (regions[noRegion].getOwner() == regions[toRegion].getOwner()
		&& regions[noRegion].getArmies() > nbArmies)
	{
		regions[noRegion].setArmies(regions[noRegion].getArmies() - nbArmies);
		regions[toRegion].setArmies(regions[toRegion].getArmies() + nbArmies);

	}
	else if (regions[noRegion].getArmies() > nbArmies)
	{
		regions[noRegion].setArmies(regions[noRegion].getArmies() - nbArmies);
	}
}